const express = require('express');
const app = express();
const router = require('./router.js');
app.use(express.json());
const bycrypt = require('bcrypt');
const jwt = require('jsonwebtoken');